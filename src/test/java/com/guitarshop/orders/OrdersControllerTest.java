package com.guitarshop.orders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(OrdersController.class)
public class OrdersControllerTest {

    private final String URI = "/api/orders";
    @Autowired
    MockMvc mockMvc;

    @MockBean
    OrdersService ordersService;

    List<Order> orders;

    private String toJSON(Object o) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(o);
    }

    @BeforeEach
    void setup() {
        orders = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Order order = new Order(1, 2.5, 1.72, "1 E Main, Maltby, WA 00000");
            orders.add(order);
        }
    }

    @Test
    void getOrders_None_ReturnsAllOrders() throws Exception {
        when(ordersService.getOrders()).thenReturn(new OrdersList(orders));
        mockMvc.perform(get(URI))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.orders", hasSize(5)));
    }

    @Test
    void getOrders_None_ReturnNoContent() throws Exception {
        when(ordersService.getOrders()).thenReturn(null);
        mockMvc.perform(get(URI))
                .andExpect(status().isNoContent());
    }

    @Test
    void addOrder_Order_ReturnNewOrder() throws Exception {
        Order order = orders.get(0);
        when(ordersService.addOrder(any(Order.class))).thenReturn(order);
        mockMvc.perform(post(URI, Order.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(order)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("customerId").value("1"));
    }

    @Test
    void addOrder_Order_BadRequest() throws Exception {
        Order order = orders.get(0);
        when(ordersService.addOrder(any(Order.class))).thenReturn(order);
        mockMvc.perform(post(URI, Order.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content("\"improper\":\"json\","))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getOrder_OrderId_ReturnsOrder() throws Exception {
        Order order = orders.get(0);
        order.setOrderId(1);
        when(ordersService.getOrderById(anyLong())).thenReturn(order);

        mockMvc.perform(get(URI + "/" + order.getOrderId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("orderId").value(order.getOrderId()));
    }

    @Test
    void getOrder_OrderId_NoContent() throws Exception {
        when(ordersService.getOrderById(anyLong())).thenReturn(null);

        mockMvc.perform(get(URI + "/" + 1234567890))
                .andExpect(status().isNoContent());
    }

    @Test
    void updateOrder_shippingAddress_ReturnsUpdatedOrder() throws Exception {
        Order order = orders.get(0);
        order.setOrderId(1);
        order.setShipAddress("2 W Main, Maltby, WA 00000");
        UpdateOrderRequest request = new UpdateOrderRequest(order.getShipAddress(), order.getBillingAddress(), order.getShipDate());

        when(ordersService.updateOrderById(anyLong(), anyString(), anyString(), any())).thenReturn(order);

        mockMvc.perform(patch(URI + "/" + order.getOrderId(), UpdateOrderRequest.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("orderId").value(order.getOrderId()));
    }

    @Test
    void updateOrder_shippingAddress_ReturnsNoContent() throws Exception {
        Order order = orders.get(0);
        order.setOrderId(1);
        order.setShipAddress("2 W Main, Maltby, WA 00000");
        UpdateOrderRequest request = new UpdateOrderRequest(order.getShipAddress(), order.getBillingAddress(), order.getShipDate());

        when(ordersService.updateOrderById(anyLong(), anyString(), anyString(), any())).thenReturn(null);

        mockMvc.perform(patch(URI + "/" + order.getOrderId(), UpdateOrderRequest.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content(toJSON(request)))
                .andExpect(status().isNoContent());
    }

    @Test
    void updateOrder_shippingAddress_BadRequest() throws Exception {
        when(ordersService.updateOrderById(anyLong(), anyString(), anyString(), any())).thenReturn(new Order());

        mockMvc.perform(patch(URI + "/" + 1, UpdateOrderRequest.class)
                .contentType(MediaType.APPLICATION_JSON)
                .content("\"improper\":\"json\","))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteOrder_OrderId_ReturnsAccepted() throws Exception {
        Order order = orders.get(0);
        order.setOrderId(1);

        mockMvc.perform(delete(URI + "/" + order.getOrderId()))
                .andExpect(status().isAccepted());
        verify(ordersService).deleteOrder(anyLong());
    }

    @Test
    void deleteOrder_OrderId_NoContent() throws Exception {
        Order order = orders.get(0);
        order.setOrderId(1);

        doThrow(new OrderNotFoundException()).when(ordersService).deleteOrder(anyLong());

        mockMvc.perform(delete(URI + "/" + order.getOrderId()))
                .andExpect(status().isNoContent());
    }
}
