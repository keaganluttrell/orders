package com.guitarshop.orders;

import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class OrdersService {

    OrdersRepository ordersRepository;

    public OrdersService(OrdersRepository ordersRepository) {
        this.ordersRepository = ordersRepository;
    }

    public OrdersList getOrders() {
        List<Order> foundOrders = ordersRepository.findAll();
        if (foundOrders.isEmpty()) return null;
        return new OrdersList(foundOrders);
    }

    public Order addOrder(Order order) {
        return ordersRepository.save(order);
    }

    public Order getOrderById(Long orderId) {
        return ordersRepository.findById(orderId).orElse(null);
    }

    public Order updateOrderById(long orderId, String shipAddress, String billAddress, Date shipDate) {
        Optional<Order> foundOrder = ordersRepository.findById(orderId);
        if (foundOrder.isEmpty()) return null;
        foundOrder.get().setShipAddress(shipAddress);
        foundOrder.get().setBillingAddress(billAddress);
        foundOrder.get().setShipDate(shipDate);
        return ordersRepository.save(foundOrder.get());
    }

    public void deleteOrder(Long orderId) {
        Optional<Order> foundOrder = ordersRepository.findById(orderId);
        if (foundOrder.isEmpty()) throw new OrderNotFoundException();
        ordersRepository.delete(foundOrder.get());
    }
}
